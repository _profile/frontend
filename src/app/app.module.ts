import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MatIconModule, MatButtonModule, MatCheckboxModule, MatInputModule, MatRippleModule, MatFormFieldModule, MatSidenavModule, MatListModule } from '@angular/material';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SidebarComponent,
   
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    BrowserAnimationsModule,
    MatButtonModule,   
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports:[  
    MatButtonModule,   
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule
  ]
})
export class AppModule { }
